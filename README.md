# OpenML dataset: The-Tweets-of-Wisdom

https://www.openml.org/d/43507

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
In the last few years, Twitter became one of the most popular social media platforms. From celebrity status to government policies, Twitter can accommodate a diverse range of people and thoughts. In these diverse set of thoughts, there are many Twitter accounts who tweet "self-help" thoughts often. These so-called "wise" thoughts are often related to improving one's life and how to excel at what you're doing. So I went down to the rabbit-hole to search these sorts of tweets. I find many common themes between them. Therefore, I decided to scrap the tweets so that you can explore the words of these "self-help" tweets and understand them much better. 
Content
I scraped the data using Tweepy API. I have scraped all the tweets, retweets and retweets with a comment of 40 authors. The data contains more than 40 authors because every retweet from any of the 40 authors is stored as a tweet from the original author.  Also, every retweet with a comment contains  and   tags. The author's comment is followed by  tag and then the content of the retweet comes which is followed by . The script I used for scrapping can be found here.
Acknowledgements
I would like to thanks Stack Overflow which helped me at literally every stage of this project from scrapping to data analysis. Also kudos to the Tweepy API which made it far more easier to fetch tweets.  
Inspiration
I downloaded this dataset for many reasons. The most important one is that I want to know how similar these tweets are. Also, I like to know what makes some tweets viral and what factors affect a viral tweet. I explore these and many more questions in my kernel which you can find in the kernel section. 
Contact Me

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43507) of an [OpenML dataset](https://www.openml.org/d/43507). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43507/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43507/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43507/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

